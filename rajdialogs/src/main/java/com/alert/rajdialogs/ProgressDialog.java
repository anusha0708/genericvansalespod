package com.alert.rajdialogs;

import android.content.Context;
import android.graphics.PorterDuff;
import androidx.core.content.ContextCompat;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by rajkumar on 23/08/17.
 */

public class ProgressDialog extends DialogBuilder<ProgressDialog> {

    private ProgressBar progressBar;
    private RelativeLayout dialogBody;
    private TextView dialog_title, dialog_message;

    public ProgressDialog(Context context) {
        super(context);

        setColoredCircle(R.color.white);
        setProgressBarColor(R.color.dialogErrorBackgroundColor);
    }

    {
        progressBar = findView(R.id.dialog_progress_bar);
        dialogBody = findView(R.id.dialog_body);
        dialog_title = findView(R.id.dialog_title);
        dialog_message = findView(R.id.dialog_message);
    }

    public ProgressDialog setDialogBodyBackgroundColor(int color) {
        if (dialogBody != null) {
            dialogBody.getBackground().setColorFilter(ContextCompat.getColor(getContext(), color), PorterDuff.Mode.SRC_IN);
        }

        return this;
    }

    public ProgressDialog setProgressBarColor(int color) {
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), color), PorterDuff.Mode.SRC_IN);
        return this;
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_progress;
    }


    public void setTitle(String title) {
        if (title != null && title.isEmpty()) {
            dialog_title.setText(title);
        }
    }

    public void setMessage(String message) {
        if (message != null && message.isEmpty()) {
            dialog_message.setText(message);
        }
    }
}
