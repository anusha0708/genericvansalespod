package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class DriverIdMainDO implements Serializable {

    public String driverName = "";
    public String locationType          = "";

    public ArrayList<DriverDO> driverDOS = new ArrayList<>();

    public ArrayList<DriverEmptyDO> driverEmptyDOS = new ArrayList<>();

}
