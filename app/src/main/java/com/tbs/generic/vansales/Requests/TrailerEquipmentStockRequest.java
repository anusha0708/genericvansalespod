package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.Model.LoadStockMainDO;
import com.tbs.generic.vansales.Model.TrailerSelectionDO;
import com.tbs.generic.vansales.Model.TrailerSelectionMainDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;

public class TrailerEquipmentStockRequest extends AsyncTask<String, Void, Boolean> {

    private TrailerSelectionMainDO trailerSelectionMainDO;
    private TrailerSelectionDO trailerSelectionDO;
    private Context mContext;
    PreferenceUtils preferenceUtils;
    String routingId;

    public TrailerEquipmentStockRequest(String routingId, Context mContext) {
        this.mContext = mContext;
        this.routingId = routingId;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, TrailerSelectionMainDO loadStockMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        JSONObject jsonObject = new JSONObject();
        try {
            ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(mContext).getActiveDeliveryMainDo(mContext);
            CustomerDo customerDo = StorageManager.getInstance(mContext).getCurrentSpotSalesCustomer(mContext);


            jsonObject.put("I_YVEHROU", routingId);
//            String ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, mContext.getResources().getString(R.string.checkin_non_scheduled));
//
//            if (ShipmentType.equals(mContext.getResources().getString(R.string.checkin_non_scheduled))) {
//
//                jsonObject.put("I_XBPC", customerDo.customer);
//            } else {
//                jsonObject.put("I_XBPC", activeDeliverySavedDo.customer);
//            }


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.TRAILER_EQUIPMENT_STOCK, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            trailerSelectionMainDO = new TrailerSelectionMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        trailerSelectionMainDO.trailerSelectionDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        trailerSelectionDO = new TrailerSelectionDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {

                        if (attribute.equalsIgnoreCase("O_YITMCODE")) {
                            trailerSelectionDO.trailer = text;


                        } else if (attribute.equalsIgnoreCase("O_YITMDES")) {
                            trailerSelectionDO.trailerDescription = text;


                        } else if (attribute.equalsIgnoreCase("O_XSTU")) {
                            trailerSelectionDO.volume = Integer.valueOf(text);

                        } else if (attribute.equalsIgnoreCase("O_YITMSAU")) {
                            if(text.length()>0){

                                trailerSelectionDO.volumeUnit = text;
                            }

                        }
                       else if (attribute.equalsIgnoreCase("O_YITMWEU")) {
                            if(text.length()>0){

                                trailerSelectionDO.massUnit = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YITMFLG")) {
                            trailerSelectionDO.flag = Integer.parseInt(text);


                        }
                        text="";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        trailerSelectionMainDO.trailerSelectionDOS.add(trailerSelectionDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

       // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, trailerSelectionMainDO);
        }
    }
}