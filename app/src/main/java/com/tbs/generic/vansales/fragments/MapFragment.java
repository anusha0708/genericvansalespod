package com.tbs.generic.vansales.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tbs.generic.vansales.Activitys.CurrentMapActivity;
import com.tbs.generic.vansales.Activitys.TransactionList;
import com.tbs.generic.vansales.Model.PickUpDo;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.LogUtils;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;


/**
 * Created by sandy on 2/15/2018.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = CurrentMapActivity.class.getSimpleName();
    public GoogleMap googleMap;
    private ArrayList<PickUpDo> pickUpDos;
    private View rootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        savedInstanceState = getArguments();
        pickUpDos = (ArrayList<PickUpDo>) savedInstanceState.getSerializable("RoutList");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.map, container, false);
        PreferenceUtils preferenceUtils = new PreferenceUtils(getActivity());
        pickUpDos = ((TransactionList)getActivity()).getLoadData();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapFragment.this);
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googlemap) {
        googleMap = googlemap;
        if (googleMap == null) {
            Toast.makeText(getActivity(), "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
        } else {
            googleMap.getUiSettings().setZoomControlsEnabled(true);

        }
        try {
            showLocation();
        }
        catch (Exception e) {
            Log.e(TAG, "Failed to initialize", e);
        }


    }

    private void drawMarker(LatLng point,String name,String city,String shipmentNumber, String status){
        LatLngBounds.Builder builder;
        CameraUpdate cu; // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();
        // Setting latitude and longitude for the marker
        markerOptions.position(point);
        markerOptions.position(point).title(name).snippet("City : "+city+"\n"+"Shipment: "+shipmentNumber);
        if(status.equalsIgnoreCase("Completed")){
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_green));
        }
        else {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.markers));
        }
        // Adding marker on the Google Map
        googleMap.addMarker(markerOptions);
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                LinearLayout info = new LinearLayout(getContext());
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(getContext());
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.LEFT);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(getContext());
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);

                return info;
            }
        });
    }

    private void showLocation() {
        try {
            if(pickUpDos.size()!=0){
                LatLngBounds bounds;
                LatLngBounds.Builder builder;
                Double lat = 0.0;
                Double lng =0.0;
                builder = new LatLngBounds.Builder();
                // Iterating through all the locations stored
                for(int i=0;i<pickUpDos.size();i++) {
                    lat = pickUpDos.get(i).lattitude;
                    // Getting the longitude of thel i-th location
                    lng = pickUpDos.get(i).longitude;
                    LogUtils.INSTANCE.debug("LATLONG", lat + ":" + lng);
                    // Drawing marker on the map
                    drawMarker(new LatLng(lat, lng),pickUpDos.get(i).description,
                                pickUpDos.get(i).city,pickUpDos.get(i).shipmentNumber, pickUpDos.get(i).status);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(lat,lng), 6));

                }
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.getUiSettings().setCompassEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);
                googleMap.getUiSettings().setZoomGesturesEnabled(true);
            }
        }
        catch (Exception e) {
            Log.e(TAG, "Failed to update location", e);
        }

    }

}