package com.tbs.generic.vansales.Activitys


import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.Model.VanStockDetailsDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.VehicleStockRequest
import com.tbs.generic.vansales.fragments.VehicleStockNonScheduleStockFragment
import com.tbs.generic.vansales.fragments.VehicleStockScheduleStockFragment
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.configuration_layout.*

class VehicleStcokActivity : BaseActivity(),
        VehicleStockScheduleStockFragment.OnFragmentInteractionListener,
        VehicleStockNonScheduleStockFragment.OnFragmentInteractionListener {


    private var vanStockDetails: VanStockDetailsDO? = null
    private var rlPartyActivity: RelativeLayout? = null
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView

    var flag: Int = 0
    var reading = 0.0
    private var screenType: String = ""

    override fun initialize() {
        rlPartyActivity = layoutInflater.inflate(R.layout.activity_vehicel_stock, null) as RelativeLayout
        llBody.addView(rlPartyActivity, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
        initializeControls()
        tv_title.text = getString(R.string.vehicle_stock)


        getDataFromServer()

    }

    private fun getDataFromServer() {

        try {
            val loasStck = VehicleStockRequest(this);
            loasStck.setOnResultListener { isError, vanStockDetails ->
                if (isError) {

                    Util.showToast(this, getString(R.string.error_messge))
                    finish()
                    return@setOnResultListener
                }
                if (vanStockDetails != null) {
                    this.vanStockDetails = vanStockDetails
                    Util.startNewFragment(this, R.id.container, VehicleStockScheduleStockFragment.newInstance(vanStockDetails, ResultListner { `object`, isSuccess ->

                        if (isSuccess) {
                        }
                    }), "ScheduleStockFragment", false)
                }

            }
            loasStck.execute()
        } catch (e: Exception) {
            e.localizedMessage
        }
    }

    override fun initializeControls() {
    }


    override fun onFragmentNonScheduleStockInteraction() {
        Util.startNewFragment(this, R.id.container, VehicleStockNonScheduleStockFragment.newInstance(vanStockDetails!!, ResultListner { `object`, isSuccess ->
            if (isSuccess) {
            }

        }), "NonScheduleStockFragment", false)
    }

    override fun onFragmentScheduleStockInteraction() {
        Util.startNewFragment(this, R.id.container, VehicleStockScheduleStockFragment.newInstance(this.vanStockDetails!!, ResultListner { `object`, isSuccess ->

            if (isSuccess) {
            }
        }), "ScheduleStockFragment", false)
    }





}