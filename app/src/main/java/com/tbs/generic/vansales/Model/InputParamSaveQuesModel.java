package com.tbs.generic.vansales.Model;

import java.util.ArrayList;

public class InputParamSaveQuesModel {
    private ArrayList<InspectionDO> inspectionDOS;
    private String contractNumber, preparationNumber, lineNumber, site, product, customer, mainComment, serialNumber,conductedBy;
    private String signatue = "";
    int type;
    private ArrayList<String> capturedImagesListBulk= new ArrayList<>();


    public ArrayList<InspectionDO> getInspectionDOS() {
        return inspectionDOS;
    }

    public void setInspectionDOS(ArrayList<InspectionDO> inspectionDOS) {
        this.inspectionDOS = inspectionDOS;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getPreparationNumber() {
        return preparationNumber;
    }

    public void setPreparationNumber(String preparationNumber) {
        this.preparationNumber = preparationNumber;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getMainComment() {
        return mainComment;
    }

    public void setMainComment(String mainComment) {
        this.mainComment = mainComment;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getSignatue() {
        return signatue;
    }

    public void setSignatue(String signatue) {
        this.signatue = signatue;
    }

    public String getConductedBy() {
        return conductedBy;
    }

    public void setConductedBy(String conductedBy) {
        this.conductedBy = conductedBy;
    }

    public ArrayList<String> getCapturedImagesListBulk() {
        return capturedImagesListBulk;
    }
}
