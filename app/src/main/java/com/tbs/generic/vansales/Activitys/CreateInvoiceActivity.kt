package com.tbs.generic.vansales.Activitys

import android.Manifest
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.util.Log
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Model.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.pdfs.BGInvoicePdf
import com.tbs.generic.vansales.pdfs.PreInvoicePdf
import com.tbs.generic.vansales.print.FileHelper
import com.tbs.generic.vansales.prints.PrinterConnection
import com.tbs.generic.vansales.prints.PrinterConstants
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util


class CreateInvoiceActivity : BaseActivity() {
    lateinit var tvSelection: TextView
    lateinit var dialog: Dialog
    lateinit var deliveryDO: DeliveryDO
    lateinit var deliveryMainDO: DeliveryMainDO
    lateinit var data: String
    lateinit var podDo: PodDo
    lateinit var btnInvoice: Button
    lateinit var btnPreInvoice: Button

    private val permissionRequestCode = 34
    val TAG = FileHelper::class.java.name
    lateinit var createPDFInvoiceDo: CreateInvoicePaymentDO
    lateinit var createInvoiceDO: CreateInvoiceDO
    var ticket = ""

    var fromId = 0
    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.create_invoice_screen, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        if (intent.hasExtra("Sales")) {
            fromId = intent.extras?.getInt("Sales")!!
        }
        if (intent.hasExtra("PICK_TICKET")) {
            ticket = preferenceUtils.getStringFromPreference(PreferenceUtils.PICK_TICKET, "")
        }
        if (ticket.isNotEmpty()) {

            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.PICK_TICKET, "")
            if (shipmentId.length > 0) {

                tvSelection.text = shipmentId
            } else {
                tvSelection.hint = getString(R.string.delivery_number)
                btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnInvoice.isClickable = false
                btnInvoice.isEnabled = false
            }
        } else {
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, resources.getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(resources.getString(R.string.checkin_non_scheduled), true)) {
                var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
                if (shipmentId.length > 0) {

                    tvSelection.text = shipmentId
                } else {
                    tvSelection.hint = getString(R.string.delivery_number)
                    btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnInvoice.isClickable = false
                    btnInvoice.isEnabled = false
                }
            } else {
                var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

                tvSelection.text = shipmentId
            }

        }

        if (!podDo.invoice.equals("")) {

            btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnInvoice.isClickable = false
            btnInvoice.isEnabled = false
            var selectedId = tvSelection.text.toString().trim()

            var deliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_SHIPMENT_ID, "")
            if (deliveryId.length > 0 && selectedId.equals(deliveryId)) {
                btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnInvoice.isClickable = false
                btnInvoice.isEnabled = false

            } else {
                btnInvoice.setBackgroundColor(resources.getColor(R.color.md_green))
                btnInvoice.isClickable = true
                btnInvoice.isEnabled = true
            }
        } else {
            btnInvoice.setBackgroundColor(resources.getColor(R.color.md_green))
            btnInvoice.isClickable = true
            btnInvoice.isEnabled = true
        }
    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.invoice)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        btnInvoice = findViewById<Button>(R.id.btnInvoice)
        tvSelection = findViewById<TextView>(R.id.tvSelection)
        btnPreInvoice = findViewById<Button>(R.id.btnPreInvoice)
        podDo = StorageManager.getInstance(this).getDepartureData(this)

        btnPreInvoice.setOnClickListener {
            Util.preventTwoClick(it)
//            prePrepareInvoiceCreation()
            val intent = Intent(this, InvoiceSignatureActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.putExtra("DATA", tvSelection.text.toString().trim())
            startActivityForResult(intent, 10)
        }
        btnInvoice.setOnClickListener {
            //            Util.preventTwoClick(it)
            invoiceCreation()
        }

        tvSelection.setOnClickListener {
            Util.preventTwoClick(it)

        }

    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
//            val intent = Intent(this@CreateInvoiceActivity, DashBoardActivity::class.java)
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//
//            startActivity(intent)


        }
    }

    override fun onButtonNoClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {

            finish()
        }
    }


    private fun invoiceCreation() {
        if (Util.isNetworkAvailable(this)) {

            data = tvSelection.text.toString()
            if (data.length > 0) {
                val siteListRequest = CreateInvoiceRequest("",podDo.invoiceNotes, data, this@CreateInvoiceActivity)
                siteListRequest.setOnResultListener { isError, createPaymentDO, msg ->
                    hideLoader()
                    if (createPaymentDO != null) {


                        createInvoiceDO = createPaymentDO
                        if (isError) {
                            if (msg.isNotEmpty()) {
                                showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                            } else {
                                showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.success), false)

                            }
                        } else {
                            if (createInvoiceDO.flag == 2) {
                                showToast(getString(R.string.invoice_created) + " - " + createInvoiceDO.salesInvoiceNumber)

//                            showAppCompatAlert("Success", "Invoice Created - " + createInvoiceDO.salesInvoiceNumber, "Ok", "", "SUCCESS", false)
                                //Toast.makeText(this@CreateInvoiceActivity, "Invoice Created", Toast.LENGTH_SHORT).show()
                                preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, createPaymentDO.salesInvoiceNumber)
                                preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, createPaymentDO.amount)
                                preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, createPaymentDO.o_shipmentNumber)
                                preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, createPaymentDO.salesInvoiceNumber)



                                podDo.invoice = createPaymentDO.salesInvoiceNumber
                                StorageManager.getInstance(this).saveDepartureData(this, podDo)

                                btnInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                btnInvoice.isClickable = false
                                btnInvoice.isEnabled = false

                                prepareInvoiceCreation()


                            } else if (createInvoiceDO.flag == 4) {
                                if (createInvoiceDO.message.length > 0) {
                                    showAppCompatAlert(getString(R.string.info), "" + createInvoiceDO.message, getString(R.string.ok), "", getString(R.string.success), false)

                                } else {
                                    showAppCompatAlert(getString(R.string.error), getString(R.string.inovice_not_created), getString(R.string.ok), "", getString(R.string.success), false)

                                }

                            } else {
                                if (createInvoiceDO.message.length > 0) {

                                    showAppCompatAlert(getString(R.string.info), "" + createInvoiceDO.message, getString(R.string.ok), "", getString(R.string.success), false)

                                } else {
                                    showAppCompatAlert(getString(R.string.error), getString(R.string.inovice_not_created), getString(R.string.ok), "", getString(R.string.success), false)

                                }

                            }

                        }
                    } else {
                        if(msg.isNotEmpty()){
                            showAppCompatAlert(getString(R.string.error),msg, getString(R.string.ok), "", getString(R.string.success), false)

                        }else{
                            showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.success), false)

                        }

                    }
                }

                siteListRequest.execute()
            } else {
                //   btnInvoice.isClickable=false
            }
        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }

    }

    private fun prepareInvoiceCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "")
//        var id="CDC-U101-19000091"//
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = InvoiceDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showToast(getString(R.string.unable_to_send_email))
//                        showAppCompatAlert("Error", "Unable to send Email", "Ok", "", "", false)
                        } else {
                            createPDFInvoiceDo = createPDFInvoiceDO
//                        if(createPDFInvoiceDo.email==10){
//                            createPaymentPDF(createPDFInvoiceDO)
//                        }
//                        if(createPaymentDo.print==10){
//                            printDocument(createPDFpaymentDO, PrinterConstants.PrintPaymentReport)
//                        }
                            if (createInvoiceDO.email == 10) {
                                createInvoicePDF(createPDFInvoiceDO)
                            }
//                            if (createInvoiceDO.print == 10) {
//                                if (checkFilePermission()) {
//                                    printDocument(createPDFInvoiceDO, PrinterConstants.PrintInvoiceReport)
//                                }
//                            }


                        }
                    } else {
                        showToast(getString(R.string.unable_to_send_email))

//                    showAppCompatAlert("Error", "Unable to send Email!!", "Ok", "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
//            showAppCompatAlert("Error", "Unable to send Email", "Ok", "", "Failure", false)
            }
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }

    private fun checkFilePermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this@CreateInvoiceActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@CreateInvoiceActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), permissionRequestCode)
        } else {
            return true
        }
        return false
    }

    private fun createInvoicePDF(createPDFInvoiceDO: CreateInvoicePaymentDO) {
        BGInvoicePdf.getBuilder(this).build(createPDFInvoiceDO, "Email")
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == permissionRequestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createInvoicePDF(createPDFInvoiceDo)
            } else {
                Log.e(TAG, "WRITE_PERMISSION_DENIED")
            }
        }

    }

    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@CreateInvoiceActivity)
        if (printConnection.isBluetoothEnabled) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast(getString(R.string.please_enable_your_bluetooth))
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().bondedDevices
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.name
                val mac = device.address // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac)
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun prePrepareInvoiceCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        if (id.isEmpty()) {
            id = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

        }
//        var id="CNV-U102-19000144"
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = PreInvoiceDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.invoice_pdf_error), getString(R.string.ok), "", "", false)
                        } else {

                            createPDFInvoiceDo = createPDFInvoiceDO
                            if (createPDFInvoiceDo.flag == 2) {
                                if (checkFilePermission()) {
                                    PreInvoicePdf.getBuilder(this).createPDF(tvSelection.text.toString().trim(), createPDFInvoiceDO, "Preview")
                                }

                            } else {
                                showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)

                            }

                        }
                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
                showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

            }

        } else {
            showAppCompatAlert(getString(R.string.error), getString(R.string.no_invoice_id_found), getString(R.string.ok), "", getString(R.string.failure), false)

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 10 && resultCode == 10) {
            podDo.paymentsTime = CalendarUtils.getCurrentDate(this)
            btnPreInvoice.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnPreInvoice.isClickable = false
            btnPreInvoice.isClickable = false
            val intent = Intent()
//            intent.putExtra("INVOICE", createPaymentDO.salesInvoiceNumber)
            setResult(10, intent)
            finish()

//            StorageManager.getInstance(this).save(this, podDo)
        }
    }


}