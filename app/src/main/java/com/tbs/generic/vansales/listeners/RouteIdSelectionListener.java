package com.tbs.generic.vansales.listeners;

public interface RouteIdSelectionListener {
    void updateId();
}
