package com.tbs.generic.vansales.Activitys


import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Adapters.LoadStockAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.NewLoadVanSaleRequest
import com.tbs.generic.vansales.Requests.NonScheduledLoadVanSaleRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.configuration_layout.*
import java.util.ArrayList

class NewLoadVanSaleStockTabActivity : BaseActivity() {
    private var rlPartyActivity: RelativeLayout? = null
    lateinit var btnLoadVehicle: Button

    lateinit var btnLoadReject: Button
    private var scheduledRootId: String = ""
    private var nonScheduledRootId: String = ""
    private var availableStockDos: ArrayList<LoadStockDO> = ArrayList()
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView

    //    lateinit var loadStockMainDO : LoadStockMainDO
    var flag: Int = 0

    private var scheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()

    override fun initialize() {
        rlPartyActivity = layoutInflater.inflate(R.layout.new_load_vansales_tab, null) as RelativeLayout
        llBody.addView(rlPartyActivity, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()
        /*toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }*/
        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
        initializeControls()
        if (intent.hasExtra("ScreenTitle")) {
            tv_title.text = intent.extras?.getString("ScreenTitle")

        } else {
            tv_title.text = getString(R.string.check_load)
        }


        if (intent.hasExtra("FLAG")) {
            flag = intent.extras!!.getInt("FLAG")
        }
        if (flag == 1) {
            btnLoadVehicle.visibility = View.GONE
            btnLoadReject.visibility = View.GONE

        } else if (flag == 2) {
            btnLoadVehicle.visibility = View.VISIBLE
            btnLoadVehicle.text =getString(R.string.unload_vehicle)
            btnLoadReject.visibility = View.GONE

        } else {
            btnLoadReject.visibility = View.GONE
            btnLoadVehicle.visibility = View.VISIBLE
        }

        val vehicleCheckInDo = StorageManager.getInstance(this@NewLoadVanSaleStockTabActivity).getVehicleCheckInData(this@NewLoadVanSaleStockTabActivity)
        scheduledRootId = vehicleCheckInDo.scheduledRootId!!
        nonScheduledRootId = vehicleCheckInDo.nonScheduledRootId!!

        if (!scheduledRootId.equals("", true)) {
//            scheduleDos = StorageManager.getInstance(this).getVanScheduleProducts(this);
        }
        if (!nonScheduledRootId.equals("", true)) {
//            nonScheduleDos = StorageManager.getInstance(this).getVanNonScheduleProducts(this);
        }

        if (scheduleDos == null || scheduleDos.size < 1) {
            loadVehicleStockData()
        } else if (nonScheduleDos == null || nonScheduleDos.size < 1) {
            loadNonScheduledVehicleStockData()
        } else {
            var isProductExisted = false

            availableStockDos = nonScheduleDos
            for (i in scheduleDos.indices) {
                for (k in availableStockDos.indices) {
                    if (scheduleDos.get(i).product.equals(availableStockDos.get(k).product, true)) {
                        availableStockDos.get(k).quantity = availableStockDos.get(k).quantity + scheduleDos.get(i).quantity
                        isProductExisted = true
                        break
                    }
                }
                if (isProductExisted) {
                    isProductExisted = false
                    continue
                } else {
                    availableStockDos.add(scheduleDos.get(i))
                }
            }
            if (availableStockDos != null && availableStockDos.size > 0) {
                var loadStockAdapter = LoadStockAdapter(this@NewLoadVanSaleStockTabActivity, availableStockDos, "Shipments")
                recycleview.adapter = loadStockAdapter
            }
        }
        btnLoadVehicle.setOnClickListener {
            Util.preventTwoClick(it)
            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
            var nID = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

            if (id.isEmpty() && nID.isEmpty()) {
                showAlert(getString(R.string.stock_not_assigne))
            } else {

                showAppCompatAlert(getString(R.string.alert), getString(R.string.are_you_sure_you_want_confirm_load), getString(R.string.ok), getString(R.string.cancel), getString(R.string.success), true)
            }


        }
    }

    override fun initializeControls() {
        btnLoadVehicle = findViewById<Button>(R.id.btnLoadVehicle) as Button

        btnLoadReject = findViewById<Button>(R.id.btnLoadReject) as Button
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        btnLoadReject.setOnClickListener {
            Util.preventTwoClick(it)
            setResult(10, null)
            finish()
        }
    }

    private fun loadVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {
            var scheduledRootId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")

            if (!scheduledRootId.equals("", true)) {
                val loadVanSaleRequest = NewLoadVanSaleRequest(scheduledRootId, this@NewLoadVanSaleStockTabActivity)
                showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast(getString(R.string.no_vehicle_data_found))
                    } else {
                        scheduleDos = loadStockMainDo.loadStockDOS

                    }
                    loadNonScheduledVehicleStockData()
                }
                loadVanSaleRequest.execute()
            } else {
                loadNonScheduledVehicleStockData()
            }
        } else {
            showToast(getString(R.string.no_internet))
        }
    }

    private fun loadNonScheduledVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {
            val nonScheduledRootId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")
            if (!nonScheduledRootId.equals("", true)) {
                val loadVanSaleRequest = NonScheduledLoadVanSaleRequest(nonScheduledRootId, this@NewLoadVanSaleStockTabActivity)
                showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast(getString(R.string.no_vehicle_data_found))
                    } else {
                        nonScheduleDos = loadStockMainDo.loadStockDOS
                        var isProductExisted = false
                        availableStockDos = nonScheduleDos
                        for (i in scheduleDos.indices) {
                            for (k in availableStockDos.indices) {
                                if (scheduleDos.get(i).product.equals(availableStockDos.get(k).product, true)) {
                                    availableStockDos.get(k).quantity = availableStockDos.get(k).quantity + scheduleDos.get(i).quantity
                                    isProductExisted = true
                                    break
                                }
                            }
                            if (isProductExisted) {
                                isProductExisted = false
                                continue
                            } else {
                                availableStockDos.add(scheduleDos.get(i))
                            }

                        }
                        val loadStockAdapter = LoadStockAdapter(this@NewLoadVanSaleStockTabActivity, availableStockDos, "Shipments")
                        recycleview.adapter = loadStockAdapter

                    }
                }
                loadVanSaleRequest.execute()
            } else {
//                tvNonScheduledStock.setText("0")
                var loadStockAdapter = LoadStockAdapter(this@NewLoadVanSaleStockTabActivity, availableStockDos, "Shipments")
                recycleview.adapter = loadStockAdapter
            }
        } else {
            showToast(getString(R.string.no_internet))
        }
    }

    override fun onButtonNoClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
//            finish()

        }
    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            StorageManager.getInstance(this).saveScheduledVehicleStockInLocal(scheduleDos)
            StorageManager.getInstance(this).saveVanScheduleProducts(this@NewLoadVanSaleStockTabActivity, scheduleDos)
            StorageManager.getInstance(this).saveNonScheduledVehicleStockInLocal(nonScheduleDos)
            StorageManager.getInstance(this).saveVanNonScheduleProducts(this@NewLoadVanSaleStockTabActivity, nonScheduleDos)

            var vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
            vehicleCheckInDo.loadStock = resources.getString(R.string.loaded_stock)
            vehicleCheckInDo.checkinTimeCaptureStockLoadingTime = CalendarUtils.getTime()
            vehicleCheckInDo.checkinTimeCaptureStockLoadingDate = CalendarUtils.getDate()

            if (StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)) {
                refreshMenuAdapter()
                setResult(4, null)
                finish()
            } else {
                showToast(getString(R.string.unable_to_load_stock))
            }

        } else
            if (getString(R.string.failure).equals(from, ignoreCase = true)) {


                //finish()
            }

    }


}